require 'simplecov'
SimpleCov.start 'rails'

ENV["RAILS_ENV"] ||= "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require "authlogic/test_case"
require "minitest/rails"
require 'minitest/reporters'
require 'pry'
# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
require "minitest/rails/capybara"
require "minitest/pride" # colorful output
require 'mocha/mini_test'

reporter_options = { color: true, slow_count: 10 }
Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new(reporter_options)]

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.javascript_driver = :selenium

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  # fixtures :all

  # load needed fixtures only at the start of each test
  self.use_transactional_fixtures = false

  # Add more helper methods to be used by all tests here...
  def json
    ActiveSupport::JSON.decode @response.body
  end
end

class ActionController::TestCase
  include Authlogic::TestCase
  setup :activate_authlogic
end

# Database cleaner setup
DatabaseCleaner.strategy = :transaction

class ActiveSupport::TestCase
  before :each do
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
  end
end


if !defined?(Spring)
  require 'shared_connection'
end
