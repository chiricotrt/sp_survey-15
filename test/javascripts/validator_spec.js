module("Validator");

test('Check Missing Params', 3, function() {
	var validator = new smart.Validator();

	raises(function(){validator.run(null);}, Error, "Must throw error on null first argument");
	raises(function(){validator.run([]);}, Error, "Must throw error on empty array as first argument");
	raises(function(){validator.run(["speed"]);}, Error, "Must throw error if current stop is not provided");

});

test('Check current stop time consistance', 4, function() {
	var validator = new smart.Validator();
	var current_stop;


	validator.setStatus("validateSpeedingCurrentNext", false);
	validator.setStatus("validateSpeedingCurrentPrevious", false);

	current_stop = {start_time: "-1:08:59", end_time: "0:08:59"};
	var validator_success = validator.run(['start_time'], current_stop);
	ok( validator_success.length === 0 , "Valid Time should not return error");

	current_stop = {start_time: "0:08:59", end_time: "-1:08:59"};
	var validator_fail = validator.run(['start_time'], current_stop);
	equal( validator_fail[0].errorCode , 1000, "Invalid time should return errorCode 1000");

	current_stop = { "start_time": "0:08:59", "end_time": "0:08:59" };
	var validator_fail1 = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_fail1.length === 1, "This test should only return 1 error");
	equal(validator_fail1[0].errorCode, 1003, "Activity with the same start and end time must return errorCode 1003");
});


test('Check time consistance between current and previous stop', 6, function(){
	var current_stop, previous_stop;
	var validator = new smart.Validator();


	previous_stop = { "start_time": "0:08:56", "end_time": "0:08:57", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965", "mode": "mode-foot"};
	var validator_success = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_success.length === 0 , "Valid Time should not return error");

	previous_stop = { "start_time": "-1:23:56", "end_time": "0:08:57", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965", "mode": "mode-foot"};
	var validator_success1 = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	equal( validator_success1.length === 0 , true, "Activity starting in the previous day with valid time should not return error");

	previous_stop = { "start_time": "0:08:56", "end_time": "0:09:00", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965", "mode": "mode-foot"};
	var validator_fail = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_fail.length === 1 , "This test should only return 1 error");
	equal( validator_fail[0].errorCode , 1001, "Activities with overlapping end and start times should return errorCode 1001");

	previous_stop = { "start_time": "0:08:56", "end_time": "0:14:00", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965", "mode": "mode-foot"};
	var validator_fail1 = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok(  validator_fail1.length === 1, "Overlapping activities test should only return exactly 1 error");
	equal(validator_fail1[0].errorCode, 1001, "Overlapping activities times should return errorCode 1001");

});

test('Check time consistance between current and next stop', 6, function(){
	var current_stop,next_stop;
	var validator = new smart.Validator();

	validator.setStatus("validateSpeedingCurrentNext", false);
	validator.setStatus("validateSpeedingCurrentPrevious", false);

	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32" };
	next_stop = { "start_time": "0:13:45", "end_time": "0:13:50" };
	var validator_success = validator.run(['start_time', 'end_time'], current_stop, null, next_stop);
	ok( validator_success.length === 0 , "Valid Time should not return error");

	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32" };
	next_stop = { "start_time": "0:13:45", "end_time": "1:13:50" };
	var validator_success1 = validator.run(['start_time', 'end_time'], current_stop, null, next_stop);
	equal( validator_success1.length === 0 , true, "Activity finishing in the next day with valid time should not return error");

	current_stop = { "start_time": "0:08:59", "end_time": "0:14:32" };
	next_stop = { "start_time": "0:13:45", "end_time": "0:13:50" };
	var validator_fail = validator.run(['start_time', 'end_time'], current_stop, null, next_stop);
	ok( validator_fail.length === 1 , "This test should only return 1 error");
	equal( validator_fail[0].errorCode , 1002, "Activities with overlapping start and end times should return errorCode 1002");


	current_stop = { "start_time": "0:08:59", "end_time": "0:13:32" };
	next_stop = { "start_time": "0:07:45", "end_time": "1:13:50" };
	var validator_fail1 = validator.run(['start_time', 'end_time'], current_stop, null, next_stop);
	ok(  validator_fail1.length === 1, "Overlapping activities test should only return exactly 1 error");
	equal(validator_fail1[0].errorCode, 1002, "Overlapping activities times should return errorCode 1002");
});


test('Check speeding between stops', 6, function(){
	var current_stop, previous_stop;
	var validator = new smart.Validator();

	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", "stop_lat": "1.3014421", "stop_lng": "103.7758439", "mode": "mode-foot"};
	var validator_success = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_success.length === 0 , "Valid Time should not return error");

	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504" };
	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", stop_lat: "1.2843661415078607", stop_lng: "103.79567078903801", "mode": "mode-car" };
	var validator_fail = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_fail.length === 0, "Valid travel time between stops for car");

	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504" };
	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", stop_lat: "1.2843661415078607", stop_lng: "103.79567078903801", "mode": "mode-foot" };
	var validator_fail1 = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok(  validator_fail1.length === 1, "Same travel time between stops with foot as mode returns error");
	equal( validator_fail1[0].errorCode, 1010, "Short travel time for the selected mode should return errorCode 1010");

	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504" };
	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", stop_lat: "1.2843661415078607", stop_lng: "103.79567078903801", "mode": "mode-foot" };
	var validator_fail2 = validator.run(['activity_location'], current_stop, previous_stop);
	ok(  validator_fail2.length === 1, "Same travel time as foot. Binded to lat and lng.");
	equal( validator_fail2[0].errorCode, 1010, "Should return errorCode 1010");
});

test('Check activity duration', 6, function(){
	var current_stop;
	var validator = new smart.Validator();

	current_stop = { "start_time": "0:08:59", "end_time": "0:18:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504", "mainactivity": "activity-work" };
	var validator_success1 = validator.run(['activity'], current_stop);
	ok( validator_success1.length === 0, "Valid acitvity duration. Binded to activity");

	current_stop = { "start_time": "0:08:59", "end_time": "0:18:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504", "mainactivity": "activity-work" };
	var validator_success2 = validator.run(['start_time', 'end_time'], current_stop);
	ok( validator_success2.length === 0, "Valid acitvity duration. Binded to time");

	current_stop = { "start_time": "0:08:59", "end_time": "0:09:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504", "mainactivity": "activity-work" };
	var validator_fail1 = validator.run(['activity'], current_stop);
	ok (validator_fail1.length === 1, "Work duration too short. Binded to activity");
	equal( validator_fail1[0].errorCode, 1020, "Should return errorCode 1020");

	current_stop = { "start_time": "0:08:59", "end_time": "0:09:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504", "mainactivity": "activity-work" };
	var validator_fail2 = validator.run(['start_time', 'end_time'], current_stop);
	ok (validator_fail2.length === 1, "Work duration too short. Binded to time");
	equal( validator_fail2[0].errorCode, 1020, "Should return errorCode 1020");
});

test('Check zero time activity duration', 3, function(){
	var current_stop;
	var validator = new smart.Validator();

	current_stop = { "start_time": "0:08:59", "end_time": "0:18:32", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504"};
	var validator_success = validator.run(['start_time', 'end_time'], current_stop);
	ok( validator_success.length === 0, "Valid acitvity duration. Binded to start_time, end_time");

	current_stop = { "start_time": "0:08:59", "end_time": "0:08:59", stop_lat: "1.318790592238002", stop_lng: "103.76393854310504"};
	var validator_fail = validator.run(['start_time', 'end_time'], current_stop);
	ok( validator_fail.length === 1, "Invalid activity duration - can't be zero. Binded to start_time, end_time");
	equal( validator_fail[0].errorCode, 1003, "Should return errorCode 1003");
});

test('Check zero time travel duration', 3, function(){
	var current_stop, previous_stop;
	var validator = new smart.Validator();

	validator.deactivateAll();
	validator.setStatus("validateZeroTimeTravels", true);

	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32", "stop_lat": "1.304889665131909", "stop_lng": "103.77269327332965" };
	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", "stop_lat": "1.3014421", "stop_lng": "103.7758439", "mode": "mode-foot"};
	var validator_success = validator.run(['start_time', 'end_time'], current_stop,previous_stop);
	ok( validator_success.length === 0, "Valid travel duration. Binded to start_time, end_time");


	previous_stop = { "start_time": "0:08:59", "end_time": "0:13:32"};
	current_stop = { "start_time": "0:13:32", "end_time": "0:13:50"};
	var validator_fail = validator.run(['start_time', 'end_time'], current_stop, previous_stop);
	ok( validator_fail.length === 1, "Invalid travel duration - can't be zero. Binded to start_time, end_time");
	equal( validator_fail[0].errorCode, 1030, "Should return errorCode 1030");
});

test('Check marker move', 2, function(){
	var current_stop;
	var validator = new smart.Validator();

	current_stop = { "start_time": "0:13:45", "end_time": "0:13:50", "stop_lat": "1.3014421", "stop_lng": "103.7758439","previous_stop_lat": "1.3014421", "previous_stop_lng": "110.7758439"};
	var validator_fail = validator.run(['activity_location'], current_stop);
	ok( validator_fail.length === 1, "User can't move the marker more than 500m away from its position");
	equal( validator_fail[0].errorCode, 1040, "Should return errorCode 1040");
});

var current_stop = {
    "index": "15482400",
    "mode": "mode-foot-15482400",
    "validated": false,
    "stop_real_id": "15482400",
    "stop_is_validated": "false",
    "stop_lat": "1.304889665131909",
    "stop_lng": "103.77269327332965",
    "no_persons_15482400": "0",
    "mainactivity": "activity-work",
    "activities": ["activity-work"],
    "start_time": "0:08:59",
    "end_time": "0:13:32"
};

var previous_stop = {
    "index": "15482399",
    "mode": "",
    "validated": false,
    "stop_real_id": "15482399",
    "stop_is_validated": "false",
    "stop_lat": "1.3084455",
    "stop_lng": "103.7748572",
    "no_persons_15482399": "0",
    "mainactivity": "activity-change_mode",
    "activities": ["activity-change_mode"],
    "start_time": "0:08:56",
    "end_time": "0:08:57"
};

var next_stop = {
    "index": "15482414",
    "mode": "mode-foot-15482414",
    "validated": false,
    "stop_real_id": "15482414",
    "stop_is_validated": "false",
    "stop_lat": "1.3014421",
    "stop_lng": "103.7758439",
    "no_persons_15482414": "0",
    "mainactivity": "activity-change_mode",
    "activities": ["activity-change_mode"],
    "start_time": "0:13:45",
    "end_time": "0:13:50"
};

window.smart.Diary._modesByImage = {};
smart.Diary._modesByImage["mode-foot"]= { "id": "4", "name": "Foot", "max_avg_speed": "5"};
smart.Diary._modesByImage["mode-car"]= { "id": "3", "name": "Car/Van", "max_avg_speed": "33"};
smart.Diary._modesByImage["mode-bus"]= { "id": "2", "name": "Bus", "max_avg_speed": "33"};
smart.Diary._modesByImage["mode-mrt"]= { "id": "6", "name": "LRT/MRT", "max_avg_speed": "33"};
smart.Diary._modesByImage["mode-bike"]= { "id": "1", "name": "Bicycle", "max_avg_speed": "7"};
smart.Diary._modesByImage["mode-taxi"]= { "id": "7", "name": "Taxi", "max_avg_speed": "33"};
smart.Diary._modesByImage["mode-moto"]= { "id": "5", "name": "Motorcycle/Scooter", "max_avg_speed": "33"};
smart.Diary._modesByImage["mode-other"]= { "id": "9", "name": "Other", "max_avg_speed": "33"};

window.smart.Diary._activitiesByImage = {};
smart.Diary._activitiesByImage["activity-default"]= { "id": "17", "name": "Default", "min_time": "", "max_time": ""};
smart.Diary._activitiesByImage["activity-home"]= { "id": "1", "name": "Home", "min_time": "15.0", "max_time": "585.0"};
smart.Diary._activitiesByImage["activity-otherhome"]= { "id": "20", "name": "Other's Home", "min_time": "15.0", "max_time": "585.0"};
smart.Diary._activitiesByImage["activity-work"]= { "id": "4", "name": "Work", "min_time": "60.0", "max_time": "900.0"};
smart.Diary._activitiesByImage["activity-shopping"]= { "id": "2", "name": "Shopping", "min_time": "5.0", "max_time": "480.0"};
smart.Diary._activitiesByImage["activity-personal"]= { "id": "14", "name": "Personal Errand/Task", "min_time": "10.0", "max_time": "420.0"};
smart.Diary._activitiesByImage["activity-medical"]= { "id": "10", "name": "Medical/Dental (Self)", "min_time": "20.0", "max_time": "330.0"};
smart.Diary._activitiesByImage["activity-sport"]= { "id": "13", "name": "Sports/Exercise", "min_time": "15.0", "max_time": "390.0"};
smart.Diary._activitiesByImage["activity-change_mode"]= { "id": "6", "name": "Change Mode/Transfer", "min_time": "1.0", "max_time": "60.0"};
smart.Diary._activitiesByImage["activity-recreation"]= { "id": "9", "name": "Recreation", "min_time": "30.0", "max_time": "420.0"};
smart.Diary._activitiesByImage["activity-school"]= { "id": "8", "name": "Education", "min_time": "60.0", "max_time": "645.0"};
smart.Diary._activitiesByImage["activity-work_related"]= { "id": "7", "name": "Work-Related Business", "min_time": "11.0", "max_time": "650.0"};
smart.Diary._activitiesByImage["activity-dining"]= { "id": "11", "name": "Meal/Eating Break", "min_time": "10.0", "max_time": "245.0"};
smart.Diary._activitiesByImage["activity-socializing"]= { "id": "5", "name": "Social", "min_time": "15.0", "max_time": "540.0"};
smart.Diary._activitiesByImage["activity-toaccompany"]= { "id": "15", "name": "To Accompany Someone", "min_time": "5.0", "max_time": "506.0"};
smart.Diary._activitiesByImage["activity-entertainement"]= { "id": "12", "name": "Entertainment", "min_time": "30.0", "max_time": "360.0"};
smart.Diary._activitiesByImage["activity-dropoff"]= { "id": "3", "name": "Pick Up/Drop Off", "min_time": "1.0", "max_time": "60.0"};
smart.Diary._activitiesByImage["activity-other"]= { "id": "19", "name": "Other", "min_time": "", "max_time": ""};
