require 'exporter_service_logger'

# This service exports data for analysis.
# The following data will be exported:
# ** PRE-SURVEY
# ** STATED-PREFERENCE EXPERIMENT
# ************* IMPORTANT ****************************
# Timestamp when SP was changed: 08/06/2018 - 15:25  *
# ****************************************************
## Sample call:
# Exporter.new.tourists(1, '2018-05-01')
# Exporter.new.residents(3, '2018-05-01')
class Exporter

  # NULL-value constants
  NULL_VALUE_RESPONSE_MISSING = -1
  NULL_VALUE_QUESTION_NOT_SHOWN = -2
  NULL_VALUE_FOR_NULL_MODE = -2

  NULL_VALUE_MISSING_VALUE = -1
  NULL_VALUE_NOT_ASKED = -2

  # Export files and columns
  EXPORT_FOLDER = "export"

  ### RESIDENTS ###
  ## COMMON ##
  RESIDENT_COLUMNS = %w(umove_id time_start time_end time_work date_started day_started date_completed day_completed total_dur time_introsp time_exitsp time_attitudessp time_exit browser device step_2)

  ## PRE-SURVEY ##
  PRE_SURVEY_FILE = "#{EXPORT_FOLDER}/#{Rails.env}/residents_rp_data_%s.csv" # city

    # SOCIO-ECONOMIC #
    PRE_SURVEY_SOCIO_ECONOMIC_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/socio_economic_characteristics.csv"
    SOCIO_ECONOMIC_CHARACTERISTICS_COLUMNS = %w(page psex pedu pemploy pemployflex pdisability pdisbus pdismetro pdistram pdisrail pdistaxi pdisbike pdiswalk)

    # HOUSEHOLD #
    PRE_SURVEY_HOUSEHOLD_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/household_characteristics.csv"
    HOUSEHOLD_CHARACTERISTICS_COLUMNS = %w(phhsize phhspouse phhparent phhchild phhgrandchild phhotherrel phhhelper phhother phhnoansw phhkids6 phhkids12 phhkids17 phhincome time_p1)

    # PRIVATE MOBILITY #
    PRE_SURVEY_PRIVATE_MOBILITY_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/private_mobility_characteristics.csv"
    PRIVATE_MOBILITY_CHARACTERISTICS_COLUMNS = %w(pliccarfull plicmotfull plicnone phhveh phhvehnum phhdriver phhvehfreq phhvehpark pcaratt1 pcaratt2 pcaratt3 pcaratt4 pcaratt5 pcaratt6 pcaratt10 pbikeown pbikefreq time_p2)

    # PUBLIC TRANSPORT MOBILITY #
    PRE_SURVEY_PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/public_transport_mobility_characteristics.csv"
    PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_COLUMNS = %w(pptfreq pptnouse1 pptnouse2 pptnouse3 pptnouse4 pptnouse5 pptnouse6 pptnouse7 pptnouse8 pptnouse9 pmodetopt ppassyes ppassno pptpasscost ppassfree ppassdiscount ppassnodiscount time_p3)

    # SHARED MOBILITY #
    PRE_SURVEY_SHARED_MOBILITY_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/shared_mobility_characteristics.csv"
    SHARED_MOBILITY_CHARACTERISTICS_COLUMNS = %w(pshareaware pccmember pccfreq pcccost pbsaware pbsfreq pbsmember ptaxihail ptaxiapp time_p4)

    # JOURNEY PLANNER / APP-BASED MOBILITY #
    PRE_SURVEY_JOURNEY_PLANNER_CHARACTERISTICS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/app_based_mobility_characteristics.csv"
    JOURNEY_PLANNER_CHARACTERISTICS_COLUMNS = %w(psmartfreq psmart_jp psmart_nav psmart_taxi psmart_rail psmart_bs psmart_cs psmartact1 psmartact2 psmartact3 psmartact7 psmartact4 psmartact5 pappatt1 pappatt2 pappatt5 pappatt6 time_p5)

    # TRAVEL PATTERN #
    PRE_SURVEY_TRAVEL_PATTERNS_FILE = 
      "#{EXPORT_FOLDER}/pre_survey/%s/travel_patterns.csv"
    TRAVEL_PATTERNS_COLUMNS = %w(phome_lon phome_lat phome_pc phome_address pwork_lon pwork_lat pwork_pc pwork_address pshop_lon pshop_lat pshop_pc pshop_address pleis_lon pleis_lat pleis_pc pleis_address pmodework pttwork pfacmode1_work pfacmode2_work pfacmode3_work pfacmode4_work pfacmode5_work pfacmode6_work pmodeshop pttshop pfacmode1_shop pfacmode2_shop pfacmode3_shop pfacmode4_shop pfacmode5_shop pfacmode6_shop pmodeleisure pttleisure pfacmode1_leis pfacmode2_leis pfacmode3_leis pfacmode4_leis pfacmode5_leis pfacmode6_leis ptcalls ptmusic ptwork ptgames ptbooks pteat ptsleep ptnoth ptother pcardfreq pcarpreq pcarsfreq pbusfreq ptramfreq pmetrofreq psrailfreq prailfreq pwalkfreq pbikefreq pbsfreq ptaxifreq ptaxiappfreq phap1 phap2 phap3 phap4 phap4 phap5 phap6 pintermod1 pintermod2 pintermod3 pintermod4 pintermod5 pcaruber time_p6)

    # SP - INTRO #
    RESIDENT_SP_INTRO_COLUMNS = %w(pm_down pm_nodown1 pm_nodown2 pm_nodown3 pm_nodown4 pm_nodown5 pfeat_jp_1 pfeat_jp_2 pfeat_jp_3 pfeat_jp_4 pfeat_jp_5 pfeat_jp_6 pfeat_oth_1 pfeat_oth_2 pfeat_oth_3 pfeat_oth_4 pfeat_oth_5 pfeat_oth_6)    
    # SP - EXIT #
    RESIDENT_SP_EXIT_COLUMNS = %w(pm_ex_feel1 pm_ex_feel2 pm_ex_feel3 pm_ex_feel4 pm_ex_feel5 pm_ex_feel6 pm_ex_feel7 pm_ex_feel8 pm_ex_feel9 pm_ex_car1 pm_ex_car2 pm_ex_car3 pm_ex_car4 pm_ex_car5 pm_ex_car6 pm_ex_nocar1 pm_ex_nocar2 pm_ex_pt pm_ex_rail pm_ex_car pm_ex_bike pm_ex_walk pm_ex_taxiapp pm_ex_taxi pm_ex_rcar cc_most cc_least wave2_part wave2_fg)

  ## SP ##
  RESIDENT_SP_FILE = "#{EXPORT_FOLDER}/residents_sp_data_%s.csv" # city
    RESIDENT_SP_COMPANY_1_FILE = "#{EXPORT_FOLDER}/#{Rails.env}/residents_sp_data_company_1_%s.csv" # city
    RESIDENT_SP_COMPANY_1_GENERAL_COLUMNS = %w(userid dataset design task taskid)
    RESIDENT_SP_COMPANY_1_PLAN_COLUMNS = %w(choice plan1_bs_cost plan1_tx_cost plan1_cs_cost plan1_fee plan2_pt plan2_bs_cost plan2_tx_cost plan2_cs_cost plan2_fee_base plan2_fee_multiplier plan2_fee plan2_contract plan3_pt plan3_bs_cost plan3_tx_cost plan3_cs_cost plan3_fee_base plan3_fee_multiplier plan3_fee plan3_contract completiontime section_completiontime chosen_pt chosen_bs chosen_tx chosen_cs chosen_fee chosen_multiplier why_not_1 why_not_2 why_not_3 why_not_4 why_not_5 why_not_6 why_not_7 why_not_8 why_not_other make_consider plan_pt_use plan_bs_use plan_tx_use plan_cs_use)

    RESIDENT_SP_COMPANY_2_FILE = "#{EXPORT_FOLDER}/#{Rails.env}/residents_sp_data_company_2_%s.csv" # city
    RESIDENT_SP_COMPANY_2_GENERAL_COLUMNS = %w(userid dataset design task taskid)
    RESIDENT_SP_COMPANY_2_PLAN_COLUMNS = %w(choice context carrot carrotdur stick plan1_pt plan1_bs plan1_combinations plan1_baseprice plan1_multiplier plan1_price plan2_bs plan2_tx plan2_combinations plan2_baseprice plan2_multiplier plan2_price plan3_tx plan3_cs plan3_combinations plan3_baseprice plan3_multiplier plan3_price completiontime section_completiontime chosen_pt chosen_bs chosen_tx chosen_cs chosen_combination chosen_price chosen_multiplier why_not_1 why_not_2 why_not_3 why_not_4 why_not_5 why_not_6 why_not_7 why_not_8 why_not_other make_consider)

    RESIDENT_SP_COMPANY_3_FILE = "#{EXPORT_FOLDER}/#{Rails.env}/residents_sp_data_company_3_%s.csv" # city
    RESIDENT_SP_COMPANY_3_GENERAL_COLUMNS = %w(userid dataset design)
    RESIDENT_SP_COMPANY_3_PLAN_COLUMNS = %w(choice_combination pt bs tx cs baseprice multiplier price buy nobuy_price completiontime)

  ### TOURIST ###
  TOURIST_COLUMNS = %w(time_start time_end time_work date_started day_started date_completed day_completed total_dur time_p1 time_p2 time_p3 time_attitudes time_introsp time_exitsp browser device city country)
  TOURIST_PRE_SURVEY_COLUMNS = %w(tage tsex tliveco temploy_tourist ttotalincome ttravelothers ttravelduration ttravelpurp ttravelmode ttravelevent tpreviour tliccarfull tlicmotfull tlicnone thometravpv thometravpvp thometravpt thometravtx thometravcy tshareaware tshareawarebike tdifficult tdifficultwhy1 tdifficultwhy2 tdifficultwhy3 tdifficultwhy4 tdifficultwhy5 tjpuse tphonedata)
  TOURIST_SP_INTRO_EXIT_COLUMNS = %w(tmaasdownload tnomaasdownload tmaasfeature1_most tmaasfeature1_least tmaasfeature2_most tmaasfeature2_least tmaasfeature3_most tmaasfeature3_least tmaasfeature4_most tmaasfeature4_least tmaasfeature5_most tmaasfeature5_least tmaasfeature6_most tmaasfeature6_least tmaasfeature7_most tmaasfeature7_least tmaasfeature8_most tmaasfeature8_least tmaasplanatt1 tmaasplanatt2 tmaasplanatt3 tmaasplanatt4 tmaasplanatt5 tmaasplanatt6 tmaasplanatt7 tmaasplanatt8)
  TOURIST_SP_GENERAL_COLUMNS = %w(userid dataset duration task) #taskid
  TOURIST_SP_PLAN_COLUMNS = %w(choice plan1_pt plan1_bs plan1_card plan1_combinations plan1_baseprice plan1_multiplier plan1_price plan2_bs plan2_tx plan2_airport plan2_hotel plan2_card plan2_combinations plan2_baseprice plan2_multiplier plan2_price plan3_tx plan3_cs plan3_airport plan3_hotel plan3_card plan3_combinations plan3_baseprice plan3_multiplier plan3_price completiontime section_completiontime chosen_pt chosen_bs chosen_tx chosen_cs chosen_combination chosen_price chosen_multiplier)

  # Exports data for tourists of {city_id} after {start_date}.
  def tourists(city_id, start_date=nil)
    EXPORTER_LOGGER.info("Running Exporter.TOURISTS")
    EXPORTER_LOGGER.info("tourists-all: ==================================================================")

    # Get all completed tourists
    users = User.where("user_type = 'tourist' and city_id = ? and created_at >= ?", city_id, start_date)
    completed_users = users.select { |user| user.tourist_survey_completed? }

    city = City.find(city_id)
    city_name = city.name.downcase
    city_prefix = city.export_tourist_prefix

    CSV.open("#{EXPORT_FOLDER}/#{Rails.env}/tourists_rp_#{city_name}.csv", "wb") do |tourist_csv|
    CSV.open("#{EXPORT_FOLDER}/#{Rails.env}/tourists_sp_#{city_name}.csv", "wb") do |tourist_sp_csv|

      tourist_csv << %w(user_id) + TOURIST_COLUMNS + TOURIST_PRE_SURVEY_COLUMNS + TOURIST_SP_INTRO_EXIT_COLUMNS
      tourist_sp_csv << TOURIST_SP_GENERAL_COLUMNS + TOURIST_SP_PLAN_COLUMNS

      completed_users.each_with_index do |user, index|
        # Generate user_id
        user_id = city_prefix + index + 1

        data = [user_id]
        TOURIST_COLUMNS.each do |column|
          data << (user.send("tourist_" + column) || NULL_VALUE_MISSING_VALUE)
        end

        TOURIST_PRE_SURVEY_COLUMNS.each do |column|
          data << (user.pre_survey.send(column) || NULL_VALUE_MISSING_VALUE)
        end

        TOURIST_SP_INTRO_EXIT_COLUMNS.each do |column|
          data << (user.sp_experiment.send(column) || NULL_VALUE_MISSING_VALUE)
        end

        tourist_csv << data

        # SP PLAN DATA #
        tasks = ["1", "2", "3"]
        duration_design = user.sp_experiment.tourist_duration

        tasks.each do |task|
          # general columns
          # userid dataset duration task
          tourist_sp_data = [user_id, city.export_dataset_value, duration_design, task]

          TOURIST_SP_PLAN_COLUMNS.each do |column|
            tourist_sp_data << user.sp_experiment.send("tourist_" + column, task)
          end

          tourist_sp_csv << tourist_sp_data
        end

      end

    end # tourist_sp_csv
    end # tourist_csv
  end

  # Exports data for residents of {city_id} after {start_date}.
  # city_id = 3
  # start_date = "2018-05-08"
  def residents(city_id, start_date=nil)
    EXPORTER_LOGGER.info("Running Exporter.RESIDENTS")
    EXPORTER_LOGGER.info("residents-all: ==================================================================")

    # Get all completed residents
    users = User.where("user_type = 'resident' and city_id = ? and created_at >= ?", city_id, start_date)
    completed_users = users.select { |user| user.resident_survey_completed? }

    city = City.find(city_id)
    city_name = city.name.downcase
    city_prefix = city.export_resident_prefix

    # Set up CSV writers
    CSV.open(PRE_SURVEY_FILE % city_name, "wb") do |presurvey_csv|
    CSV.open(PRE_SURVEY_SOCIO_ECONOMIC_CHARACTERISTICS_FILE % city_name, "wb") do |ps_socio_economic_csv|
    CSV.open(PRE_SURVEY_HOUSEHOLD_CHARACTERISTICS_FILE % city_name, "wb") do |ps_household_csv|
    CSV.open(PRE_SURVEY_PRIVATE_MOBILITY_CHARACTERISTICS_FILE % city_name, "wb") do |ps_private_mobility_csv|
    CSV.open(PRE_SURVEY_PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_FILE % city_name, "wb") do |ps_public_transport_mobility_csv|
    CSV.open(PRE_SURVEY_SHARED_MOBILITY_CHARACTERISTICS_FILE % city_name, "wb") do |ps_shared_mobility_csv|
    CSV.open(PRE_SURVEY_JOURNEY_PLANNER_CHARACTERISTICS_FILE % city_name, "wb") do |ps_journey_planner_csv|
    CSV.open(PRE_SURVEY_TRAVEL_PATTERNS_FILE % city_name, "wb") do |ps_travel_pattern_csv|
    CSV.open(RESIDENT_SP_COMPANY_1_FILE % city_name, "wb") do |sp_c1_csv|
    CSV.open(RESIDENT_SP_COMPANY_2_FILE % city_name, "wb") do |sp_c2_csv|
    CSV.open(RESIDENT_SP_COMPANY_3_FILE % city_name, "wb") do |sp_c3_csv|

      ps_socio_economic_csv << SOCIO_ECONOMIC_CHARACTERISTICS_COLUMNS
      ps_household_csv << HOUSEHOLD_CHARACTERISTICS_COLUMNS
      ps_private_mobility_csv << PRIVATE_MOBILITY_CHARACTERISTICS_COLUMNS
      ps_public_transport_mobility_csv << PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_COLUMNS
      ps_shared_mobility_csv << SHARED_MOBILITY_CHARACTERISTICS_COLUMNS
      ps_journey_planner_csv << JOURNEY_PLANNER_CHARACTERISTICS_COLUMNS
      ps_travel_pattern_csv << TRAVEL_PATTERNS_COLUMNS

      presurvey_csv << aggregated_column_headers_for_presurvey

      sp_c1_csv << RESIDENT_SP_COMPANY_1_GENERAL_COLUMNS + RESIDENT_SP_COMPANY_1_PLAN_COLUMNS
      sp_c2_csv << RESIDENT_SP_COMPANY_2_GENERAL_COLUMNS + RESIDENT_SP_COMPANY_2_PLAN_COLUMNS
      sp_c3_csv << RESIDENT_SP_COMPANY_3_GENERAL_COLUMNS + RESIDENT_SP_COMPANY_3_PLAN_COLUMNS

      # Iterate through each user and export:
      # - pre-survey data
      # - sp data (stated preference experiment) #TODO
      completed_users.each_with_index do |user, index|
        # Generate user_id
        unique_user_id = city_prefix + index + 1
        EXPORTER_LOGGER.info("user-#{user.id}: Generated unique_user_id - #{unique_user_id}")

        ## PRE-SURVEY ##
        EXPORTER_LOGGER.info("user-#{user.id}: Exporting PRE-SURVEY data ...")

        # SOCIO-ECONOMIC #
        EXPORTER_LOGGER.info("user-#{user.id}: ... SOCIO-ECONOMIC")
        psse_data = ps_socio_economic_data(user)
        ps_socio_economic_csv << psse_data
        EXPORTER_LOGGER.info("user-#{user.id}: SOCIO-ECONOMIC.")

        # HOUSEHOLD #
        EXPORTER_LOGGER.info("user-#{user.id}: ... HOUSEHOLD")
        psh_data = ps_household_data(user)
        ps_household_csv << psh_data
        EXPORTER_LOGGER.info("user-#{user.id}: HOUSEHOLD.")

        # PRIVATE MOBILITY #
        EXPORTER_LOGGER.info("user-#{user.id}: ... PRIVATE MOBILITY")
        pspm_data = ps_private_mobility_data(user)
        ps_private_mobility_csv << pspm_data
        EXPORTER_LOGGER.info("user-#{user.id}: PRIVATE MOBILITY.")

        # PUBLIC TRANSPORT MOBILITY #
        EXPORTER_LOGGER.info("user-#{user.id}: ... PUBLIC TRANSPORT MOBILITY")
        psptm_data = ps_public_transport_mobility_data(user)
        ps_public_transport_mobility_csv << psptm_data
        EXPORTER_LOGGER.info("user-#{user.id}: PUBLIC TRANSPORT MOBILITY.")

        # SHARED MOBILITY #
        EXPORTER_LOGGER.info("user-#{user.id}: ... SHARED MOBILITY")
        pssm_data = ps_shared_mobility_data(user)
        ps_shared_mobility_csv << pssm_data
        EXPORTER_LOGGER.info("user-#{user.id}: SHARED MOBILITY.")

        # JOURNEY PLANNER #
        EXPORTER_LOGGER.info("user-#{user.id}: ... JOURNEY PLANNER")
        psjp_data = ps_journey_planner_data(user)
        ps_journey_planner_csv << psjp_data
        EXPORTER_LOGGER.info("user-#{user.id}: JOURNEY PLANNER.")

        # TRAVEL PATTERN #
        EXPORTER_LOGGER.info("user-#{user.id}: ... TRAVEL PATTERN")
        pstp_data = ps_travel_pattern_data(user)
        ps_travel_pattern_csv << pstp_data
        EXPORTER_LOGGER.info("user-#{user.id}: TRAVEL PATTERN.")

        # SP - INTRO, EXIT #
        EXPORTER_LOGGER.info("user-#{user.id}: ... SP-INTRO+EXIT")
        spie_data = sp_intro_exit_data(user)
        EXPORTER_LOGGER.info("user-#{user.id}: SP-INTRO+EXIT.")

        # AGGREGATE PRE-SURVEY DATA #
        presurvey_csv <<
          ps_common_data(user, unique_user_id) +
          psse_data +
          psh_data +
          pspm_data +
          psptm_data +
          pssm_data +
          psjp_data +
          pstp_data +
          spie_data

        EXPORTER_LOGGER.info("user-#{user.id}: Exported PRE-SURVEY data.")

        # ## SP ##
        EXPORTER_LOGGER.info("user-#{user.id}: Exporting SP data ...")
        # COMPANY 1 #
        EXPORTER_LOGGER.info("user-#{user.id}: ... COMPANY 1")
        design = 1
        tasks = ["1", "2", "3"]
        tasks.each do |task|
          # general columns
          # (userid dataset design task taskid)
          sp_c1_data = [unique_user_id, city.export_dataset_value, design, task, "#{unique_user_id}0#{task}"]

          RESIDENT_SP_COMPANY_1_PLAN_COLUMNS.each do |column|
            sp_c1_data << user.sp_experiment.send("company_1_" + column, task)
          end

          sp_c1_csv << sp_c1_data
        end

        # COMPANY 2 #
        EXPORTER_LOGGER.info("user-#{user.id}: ... COMPANY 2")
        design = 2
        tasks = ["1", "2", "3", "4", "5"]
        tasks.each do |task|
          EXPORTER_LOGGER.debug "Task #{task}:"
          # general columns
          # (userid dataset design task taskid)
          sp_c2_data = [unique_user_id, city.export_dataset_value, design, task, "#{unique_user_id}0#{task}"]

          RESIDENT_SP_COMPANY_2_PLAN_COLUMNS.each do |column|
            sp_c2_data << user.sp_experiment.send("company_2_" + column, task)
          end

          sp_c2_csv << sp_c2_data
        end

        # COMPANY 3 #
        EXPORTER_LOGGER.info("user-#{user.id}: ... COMPANY 3")
        design = 3
        # general columns
        # (userid dataset design)
        sp_c3_data = [unique_user_id, city.export_dataset_value, design]

        RESIDENT_SP_COMPANY_3_PLAN_COLUMNS.each do |column|
          sp_c3_data << user.sp_experiment.send("company_3_" + column)
        end

        sp_c3_csv << sp_c3_data

        EXPORTER_LOGGER.info("user-#{user.id}: Exported SP data.")

        EXPORTER_LOGGER.info("user-#{user.id}: ----------------------------------------------------------")
      end

    end # sp_c3_csv
    end # sp_c2_csv
    end # sp_c1_csv
    end # ps_travel_pattern_csv
    end # ps_journey_planner_csv
    end # ps_shared_mobility_csv
    end # ps_public_transport_mobility_csv
    end # ps_private_mobility_csv
    end # ps_household_csv
    end # ps_socio_economic_csv
    end # presurvey_csv

    EXPORTER_LOGGER.info("residents-all: ==================================================================")
    EXPORTER_LOGGER.info("Stopped Exporter.RESIDENTS")
  end

  def aggregated_column_headers_for_presurvey
    ["user_id"] +
    RESIDENT_COLUMNS +
    SOCIO_ECONOMIC_CHARACTERISTICS_COLUMNS +
    HOUSEHOLD_CHARACTERISTICS_COLUMNS +
    PRIVATE_MOBILITY_CHARACTERISTICS_COLUMNS +
    PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_COLUMNS +
    SHARED_MOBILITY_CHARACTERISTICS_COLUMNS +
    JOURNEY_PLANNER_CHARACTERISTICS_COLUMNS +
    TRAVEL_PATTERNS_COLUMNS +
    RESIDENT_SP_INTRO_COLUMNS + RESIDENT_SP_EXIT_COLUMNS
  end

  def sp_intro_exit_data(user)
    data = []

    if user.sp_experiment
      (RESIDENT_SP_INTRO_COLUMNS + RESIDENT_SP_EXIT_COLUMNS).each do |column|
        data << (user.sp_experiment.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      (RESIDENT_SP_INTRO_COLUMNS + RESIDENT_SP_EXIT_COLUMNS).length.times do
        data << ""
      end
    end

    data
  end

  def ps_socio_economic_data(user)
    data = []

    if user.pre_survey_socio_economic_characteristic
      SOCIO_ECONOMIC_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_socio_economic_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      SOCIO_ECONOMIC_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_household_data(user)
    data = []

    if user.pre_survey_household_characteristic
      HOUSEHOLD_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_household_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      HOUSEHOLD_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_private_mobility_data(user)
    data = []

    if user.pre_survey_private_mobility_characteristic
      PRIVATE_MOBILITY_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_private_mobility_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      PRIVATE_MOBILITY_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_public_transport_mobility_data(user)
    data = []

    if user.pre_survey_public_transport_characteristic
      PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_public_transport_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      PUBLIC_TRANSPORT_MOBILITY_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_shared_mobility_data(user)
    data = []

    if user.pre_survey_shared_mobility_characteristic
      SHARED_MOBILITY_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_shared_mobility_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      SHARED_MOBILITY_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_journey_planner_data(user)
    data = []

    if user.pre_survey_journey_planner_characteristic
      JOURNEY_PLANNER_CHARACTERISTICS_COLUMNS.each do |column|
        data << (user.pre_survey_journey_planner_characteristic.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      JOURNEY_PLANNER_CHARACTERISTICS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_travel_pattern_data(user)
    data = []

    if user.pre_survey_travel_pattern
      TRAVEL_PATTERNS_COLUMNS.each do |column|
        data << (user.pre_survey_travel_pattern.send(column) || NULL_VALUE_MISSING_VALUE)
      end
    else
      TRAVEL_PATTERNS_COLUMNS.length.times do
        data << ""
      end
    end

    data
  end

  def ps_common_data(user, unique_user_id)
    data = [unique_user_id]

    RESIDENT_COLUMNS.each do |column|
      data << user.send("resident_" + column)
    end

    data
  end

  ## Exports SP data for user ##
  # TASKS n #
  def sp_simple_data(user, unique_user_id, task_labels, mode_columns, position_columns, unlimited=false)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_simple_tasks(unique_user_id, task_labels,
        mode_columns, position_columns, unlimited)
    end

    data
  end

  # TASKS x #
  def sp_medium_data(user, unique_user_id)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_medium_tasks(unique_user_id)
    end

    data
  end

  # TASK 13 #
  def sp_flexible_data(user, unique_user_id)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_flexible_task(unique_user_id)
    end

    data
  end

  # TASK F #
  def sp_final_data(user, unique_user_id)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_final_task(unique_user_id)
    end

    data
  end

  # CHOSEN SIMPLE TASKS #
  def sp_chosen_simple_data(user, unique_user_id)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_chosen_simple_tasks(unique_user_id)
    end

    data
  end

  # CHOSEN MEDIUM TASKS #
  def sp_chosen_medium_data(user, unique_user_id)
    data = []
    if user.sp_experiment
      # user.sp_experiment.where(completed: true).first.export_basic_tasks(unique_user_id)
      data += user.sp_experiment.export_chosen_medium_tasks(unique_user_id)
    end

    data
  end

end
