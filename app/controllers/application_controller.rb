# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  helper_method :current_user_session, :current_user, :logged_in?
  # filter_parameter_logging :password, :password_confirmation

  around_action :switch_locale
  before_filter :set_timezone, :if => :logged_in?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password

  rescue_from Acl9::AccessDenied, :with => :access_denied

  def switch_locale(&action)
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  def default_url_options
    { locale: I18n.locale }
  end

  private

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  def require_user
    unless current_user
      store_location
      # flash[:notice] = I18n.t('account.must_be_logged_in')
      # flash[:class] = 'content_error'
      # redirect_to pages_newuser_path
      panelist_secure_id = params[:id] || params[:Id] || params[:ID]
      
      # redirect_to new_wsp_users_path(id: panelist_secure_id)
      render file: "#{Rails.root}/public/404", status: :not_found

      return false
    end

    return true
  end

  def require_no_user
    if current_user
      store_location
      flash[:notice] = I18n.t('account.must_be_logged_out')
      flash[:class] = 'content_error'
      redirect_to root_url
      return false
    end
    return true
  end

  def require_admin
    if current_user && !current_user.roles.empty?
      admin_role = current_user.roles.find_by_name("admin")
      if admin_role
        return true
      end
    end

    flash[:notice] = I18n.t('account.must_be_admin_in')
    flash[:class] = 'content_error'
    redirect_to root_url
    return false
  end

  def require_full_day?
    if SPECIAL_USERS.include? current_user.id
      return false
    else
      if current_user.created_at >= Date.parse(FULL_DAY_USERS_DATE)
        return true
      else
        return false
      end
    end
  end

  def store_location
    session[:return_to] = request.url
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  def logged_in?
    return current_user
  end


  def random_string(length = 8)
    (0...length).map{(65+rand(26)).chr}.join
  end

  def api_auth
    @current_user = User.find_by_single_access_token(params[:token])
    access_denied if @current_user.nil?
  end

  protected

  def set_timezone
    if current_user.time_zone.present?
      zone = current_user.time_zone
    elsif session[:time_zone].present?
      zone = session[:time_zone]
    else
      zone = "UTC"
    end

    tz = Time.find_zone(zone)
    session[:time_zone] = tz.name
    session[:time_zone_tzinfo] = tz.tzinfo.name

    timezone_changed
    # puts "SETTING TIMEZONE to #{session[:time_zone]} #{session[:time_zone_tzinfo]}"
  end

  def build_date_in_timezone(date_str, time_zone)
    #estHoursOffset = Time.zone.utc_offset / 3600
    #estOffset = Rational(estHoursOffset, 24)
    #build date with string, shift time based on new timezone, change to new timezone
    #date = (DateTime.parse(date_str) - (estHoursOffset/24.0)).new_offset(estOffset)
    date = Time.find_zone(time_zone).parse(date_str)
    return date
  end

  def timezone_changed
    if cookies[:timezone]
      cookie_tz = Time.find_zone(cookies[:timezone])
      user_tz = Time.find_zone(session[:time_zone])
      if cookie_tz.utc_offset != user_tz.utc_offset
        @timezone_switch = {name: cookies[:timezone], description: cookie_tz, formatted_offset: cookie_tz.formatted_offset}
      end
    end
  end

  def access_denied
    flash[:notice] = 'Access denied: invalid login credentials or insufficient privileges'
    # redirect_to root_path
    render json: {message: flash[:notice]}, status: :unauthorized
  end
end
