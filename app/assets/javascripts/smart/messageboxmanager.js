function isStopVerified(id){
	// console.log("verifyStopNumber ",id);

	var elm = jQuery('#'+id+' input[name="stop_is_validated"]')
	if (elm.val() == 'true')
		return true;
	else
		return false;
}

function isTravelVerified(id){
	// console.log("verifyStopNumber ",id);

	var elm = jQuery('#travel'+id+' input[name="stop_is_validated"]')
	if (elm.val() == 'true')
		return true;
	else
		return false;
}

function verifyStopNumber(id){
	//console.log("verifyStopNumber ",id);

	var elm = jQuery(id)
	classname = elm.attr('class')

	//console.log("verifyStopNumber classes", classname);

	var classes = classname.split(' ')
	for(var j=0;j<classes.length;j++){
		var tmp = classes[j]
		if (tmp.indexOf('c-stop')!=-1 && tmp.indexOf('-validated')==-1){
			elm.removeClass(tmp)
			elm.addClass(tmp+'-validated')
			break;
		}
	}

}

function setGenericValidated(id, status){
	if (isRowTravel(id)){
		var result = id.match(/[\d]+/);
    	result = result[0];
    	setTravelValidatedStatus(result, status)
	} else {
		setActivityValidatedStatus(id, status)
	}
}

function setTravelValidatedStatus(id, status){
	var icon = jQuery(".travel_" + id +" .sprite");

	var travelElm = jQuery('.travel_'+id+' .validated-info');
	if (travelElm.length === 0) return; // first stop row does not have matching travel row

	toggleIconCheck(icon, status);
	jQuery('#Travel'+id+' .stop_is_validated').val(status);

	var classes = travelElm.attr('class').split(' ');
	for(var j=0;j<classes.length;j++){
		var tmp = classes[j]
		if (tmp.indexOf('status-')!=-1){
			travelElm.removeClass(tmp)
			if (status){
				travelElm.addClass('status-valid')
			} else {
				travelElm.addClass('status-invalid')
			}
			break;
		}
	}
}

function setActivityValidatedStatus(id, status){
	var icon = jQuery("#stop_number_" + id +" .sprite");
	toggleIconCheck(icon, status);

	var activityElm = jQuery('.activity_'+id+' .validated-info');
	jQuery('#'+id+' .stop_is_validated').val(status)

	var classes = activityElm.attr('class').split(' ');
	for(var j=0;j<classes.length;j++){
		var tmp = classes[j]
		if (tmp.indexOf('status-')!=-1){
			activityElm.removeClass(tmp)
			if (status){
				activityElm.addClass('status-valid')
			} else {
				activityElm.addClass('status-invalid')
			}
			break;
		}
	}
}

function toggleIconCheck(element, status){
	var icon_class = element.attr("class");
	if (status && icon_class.indexOf("-checked") < 0) {
		element.attr("class", icon_class + "-checked");
	} else if (!status && icon_class.indexOf("-checked") >= 0){
		element.attr("class", icon_class.replace("-checked", ""));
	};
}

function setGenericRowError(id, status){
	if (isRowTravel(id)){
		var result = id.match(/[\d]+/);
    	result = result[0];
    	setTravelError(result, status)
	} else {
		setActivityError(id, status)
	}
}

function setTravelError(id, status){
	//console.log("Calling Travel",arguments)
	setRowError(('.travel_'+id), status);
}

function setActivityError(id, status){
	//console.log("Calling Activity",arguments)
	setRowError(('.stop_'+id), status);
}

function unsetRowError(classnameId){
	//console.log("UNSETROWERROR", classnameId+' .validated-info')
	var Elm = jQuery(classnameId+' .validated-info');

	var classes = Elm.attr('class').split(' ')
	for(var j=0;j<classes.length;j++){
		var tmp = classes[j]
		if (tmp.indexOf('status-error')!=-1){
			Elm.removeClass(tmp)
		}
	}
}

function setRowError(classnameId, status){
	//console.log("Calling setRowError",arguments)
	var isError = status==null?true:status;
	var Elm = jQuery(classnameId+' .validated-info');

	unsetRowError(classnameId);

	//console.log("isError",isError)
	if (isError){
		console.log("Adding class error")
		Elm.addClass('status-error')
	}
}

function setStopValidatedStatus(id, validatedFlag){
	//console.log("setStatus of ",id, " st=",validatedFlag);
	var className = ""
	var doTravel = (validatedFlag & 0x2) > 0;
	var doActivity = (validatedFlag & 0x1) > 0;

	setActivityValidatedStatus(id, doActivity);
	setTravelValidatedStatus(id, doTravel);

	// call checks
	//console.log("CALL CHECK STATUS on ROWS");
}

function unVerifyStopNumber(id){
	//console.log("verifyStopNumber ",id);

	var elm = jQuery(id)
	classname = elm.attr('class')

	//console.log("verifyStopNumber classes", classname);
	if (classname === undefined) return;

	var classes = classname.split(' ')
	for(var j=0;j<classes.length;j++){
		var tmp = classes[j]
		if (tmp.indexOf('c-stop')!=-1 && tmp.indexOf('-validated')!=-1){
			elm.removeClass(tmp)
			elm.addClass(tmp.substr(0,tmp.indexOf('-validated')))
			break;
		}
	}

	//console.log("verifyStopNumber classes", elm.attr('class'));

}

function showMarker(marker_id){
	//console.log(markers[marker_id]);

	markers[marker_id].setMap(map);
}

function updateToolTip(name, tooltip){
		if (typeof toolTipHash == 'undefined' ){
			toolTipHash = {};
		}
		toolTipHash[name] = tooltip;
}

jQuery.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   jQuery.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};


//------------------ HELPER FUNCTIONS ------------------------

var processTime = function(strTime){
	if (strTime == undefined)
		return undefined

	var parts = strTime.split(':')
	return { day: parts[0], hours: parseInt(parts[1],10), minutes: parseInt(parts[2],10)}
}

var timeJoin = function(timeObj){

	return parseInt(timeObj.day)+':'+(timeObj.hours<10?'0':'')+timeObj.hours+":"+(timeObj.minutes<10?'0':'')+timeObj.minutes;
}

var timeIsLowerThan = function(a,b){
	return timeJoin(a) < timeJoin(b);
}

var timeLET = function(a,b){
	return timeJoin(a) <= timeJoin(b)
}


//-------------------- MESSAGE BOX MANAGER ----------------------

var smart;
if (smart == undefined) {
	smart = {};
}

smart.MessageBoxManager = function(){
	this.initialize();
}

smart.MessageBoxManager.prototype = {
	messagesList: {
		"valid" : "All of your travel and activities have been checked.",
		"invalid": "One or more activities have not been checked.",
		"error": "There one or more activites with problems. Please review them."
	},

	initialize: function(){
		_.bindAll(this, "initialize", "setMessageBoxStatus", "addError", "checkAllRows", "checkActivities", "validateActivities", "updateCalendarDays");

		this.messages = {};
		this.notValidated = [];
		this.errorCount = 0;

		jQuery.subscribe("msgbox:status", this.setMessageBoxStatus);
	},

	setMessageBoxStatus: function(e,status, msg){
		//console.log(arguments,msg)
		var msgText = "";
		if (msg == null){
			if (this.messagesList[status] != null){
				msgText = this.messagesList[status];
			}
		} else {
			msgText = msg;
		}

		//console.log("publish msgbox status");
		var elm = jQuery('#messageBox');

		var classes = elm.attr('class');
		if (classes === undefined) return;
		classes = classes.split(' ');
		for(var j=0;j<classes.length;j++){
			var tmp = classes[j]
			if (tmp.indexOf('status-')!=-1){
				elm.removeClass(tmp)
			}
		}
		elm.addClass('status-'+status);
		jQuery('#validation_feedback', elm).html(msgText);
		jQuery("#messageBoxWrapper").show();
	},

	addError: function(row_id, status, msg){
		if (!(row_id in this.messages))
			this.messages[row_id] = [];

		this.messages[row_id].push({'status':status, 'msg':msg});
		this.errorCount += 1;
	},

	renderErrors: function(){
		var rowIds = jQuery('form.activityrow').map(function(e,elm){return elm.id});
		firstError = true
		for(var i = 1; i<rowIds.length; i++)
		{
			if (rowIds[i] in this.messages){
				//console.log("there are error messages for "+rowIds[i]);
				var stop = this.messages[rowIds[i]];
				if (firstError){
					//console.log("set error messages");
					this.setMessageBoxStatus(null, stop[0].status, stop[0].msg);
					firstError = false;
				}

				setGenericRowError(rowIds[i], true);
			} else {
				setGenericRowError(rowIds[i], false);
				if (this.checkIfRowIsValidated(""+rowIds[i])){
					setGenericValidated(""+rowIds[i], true);
				} else {
					setGenericValidated(""+rowIds[i], false);
				}

			}
		}

		if (this.errorCount == 0) {
			//check if all valid
			this.checkIfDayIsValidated();
		}
	},

	checkIfDayIsValidated: function(){
		var self = this;
		jQuery.post(	'/report/isDayValidated',
						{day: jQuery('#datepicker_value').val() },
						function(data){
							if (data && data.result == 'true'){
								self.setMessageBoxStatus(null,"valid");
							} else {
								self.setMessageBoxStatus(null,"invalid");
							}
						},
						"json"
					).fail(function(data){
						self.setMessageBoxStatus(null, "invalid");
					})
	},

	checkAllRows: function(){
		this.messages = {};
		this.notValidated = {};
		this.errorCount = 0;
		this.checkActivities();

		if (this.errorCount != 0){
			this.updateCalendarDays();
		}
	},

	checkIfRowIsValidated: function(rowId){
		return (jQuery('#'+rowId+" .stop_is_validated").val()) == 'true'
	},


	checkActivities: function(){
		var start = 0;

		var startTimeArr = jQuery(".stop_form input.start_time_input");
		var endTimeArr = jQuery(".stop_form input.end_time_input");

		//variable counter starts in 1, thus we have to do counter - 2
		for(var i=start;i < startTimeArr.length ;i++){
			//console.log("validate i="+i+" || "+ <%= counter -2 %>);
			prevEndTime = processTime(jQuery(endTimeArr[i-1]).val())
			startTime = processTime(jQuery(startTimeArr[i]).val())

			endTime = undefined

			endTime = processTime(jQuery(endTimeArr[i]).val())
			nextStartTime = processTime(jQuery(startTimeArr[i+1]).val())

			var stopid = jQuery(startTimeArr[i]).attr('id').split('_')[2];

			// current start lower than previous end
			if ( prevEndTime != undefined && timeIsLowerThan(startTime, prevEndTime) ) {
				this.addError(""+stopid,"error", "The activity "+(i+1)+" begins before the end of activity "+(i)+".");
				continue;
			// current end is lower than start
			} else if ( endTime != undefined && timeIsLowerThan(endTime, startTime) ) {
				this.addError(""+stopid, "error", "The activity "+(i+1)+" is ending before it even started.");
				continue;
			}
		}
	},

	validateActivities: function(){
		this.checkAllRows();
		this.renderErrors();
	},

	updateCalendarDays: function(){
		jQuery.post('/report/updateCalendar',
  					{
  					},
  					function(data){
  						//eval(data);
  					});
	}

}

smart.messageBox = new smart.MessageBoxManager();
