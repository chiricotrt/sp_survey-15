var smart;
if (smart === undefined) {
	smart = {};
}

smart.backendMap = function(){
	this.initialize();
};

smart.backendMap.prototype = {
	max_gps_diff: 600,
	states_msg: {
		'1': 'gps',
		'2': 'sleep',
		'3': 'phone still'
	},

	initialize: function(){
		_.bindAll(this, "initialize", "loadData", "analyseData", "renderData", "unloadMakers", "createPoly", "createMarker",
						"showMarkers", "showPaths", "hideMarkers", "hidePaths", "show", "hide");

		this.markersList = [];
		this.pathList = [];

		this.raw_data_viz = new smart.RawDataViz({ el: ".raw_data_container" });

		this.onLoadEnd = function(){};
		jQuery.subscribe("msgbox:status", this.setMessageBoxStatus);
	},

	loadData: function(map, user_id, day){
		this.unloadMakers();
		this.raw_data_viz.unload();
		this.oms = new OverlappingMarkerSpiderfier(map, {markersWontMove: true, markersWontHide: true});

		var self = this;
		self.map = map;
		var date_parts = day.split('-');
		var dateObj = new Date(date_parts[2], date_parts[1]-1, date_parts[0]);
		jQuery.post('/backoffice/getExtraData/'+user_id, {'day': day}, function(data){
			self.states = data.states;
			self.gpspoints = data.gps;

			console.log("[backendMap] data download complete");
			self.analyseData();
			console.log("[backendMap] data analysis complete");
			self.renderData();
			//console.log("[backendMap] data render complete");
			self.onLoadEnd();

			jQuery('#time_offset').text(data.user_timezone);
			self.raw_data_viz.loadData(data);
			self.raw_data_viz.render();
		});
	},

	analyseData: function(){
		if (this.states.length < 1)
			return;

		var currentState = this.states[0];
		var currentStateDate = parseInt(currentState.timestamp,10);
		currentState.gps_diff = 10000000000;
		var currentGPSDate;
		var stateCount = 1;
		var lastGPSPoint;
		var lastDiff;
		var i;
		for(i=0; i<this.gpspoints.length; i++){
			currentGPSDate = parseInt(this.gpspoints[i].timestamp,10);
			this.gpspoints[i].parsed_timestamp = currentGPSDate
			var diff = (currentGPSDate - currentStateDate);
			//console.log(i,diff);
			//if (Math.abs(diff) <= 20000){

				//console.log("found timestamp")
			if (Math.abs(currentState.gps_diff) > Math.abs(diff))   {
				//console.log("found timestamp - " + diff/1000);
				currentState.gps = this.gpspoints[i];
				currentState.gps_idx = i;
				currentState.gps_diff = diff;

			} else if (currentStateDate < currentGPSDate){
				//console.log(stateCount);
				i = currentState.gps_idx;
				currentState = this.states[stateCount++];

				if (currentState === undefined)
					break;

				currentState.gps_diff = -10000000000;
				currentState.idx = stateCount-1;
				currentStateDate = parseInt(currentState.timestamp,10);

				//console.log("i="+i);
			}
		}

	},

	decodeLevels: function(encodedLevelsString) {
		var decodedLevels = [];

		for (var i = 0; i < encodedLevelsString.length; ++i) {
			var level = encodedLevelsString.charCodeAt(i) - 63;
			decodedLevels.push(level);
		}
		return decodedLevels;
	},

	renderData: function(){
		var tmpState;
		var tmpMarker;
		var discardedCount = 0;
		if (!this.markersList)
			this.markersList = [];

		var i;
		for (i = 0; i<this.states.length; i++){
			tmpState = this.states[i];
			if (tmpState.gps && tmpState.gps_diff < this.max_gps_diff){
				tmpMarker = this.createMarker(	parseFloat(tmpState.gps.lat, 10),
												parseFloat(tmpState.gps.lon, 10),
												{	id: tmpState.id,
													idx: tmpState.idx,
													state: tmpState.state,
													msg: this.states_msg[tmpState.state],
													offset: tmpState.gps_diff
												},
												this.map
								);
				tmpState.marker = tmpMarker;
				tmpState.marker_idx = this.markersList.length;
				this.markersList.push(tmpMarker);
			} else {
				discardedCount +=1 ;
			}
		}

		this.oms.addListener('click', function(marker) {
				tooltip.hide();
				tooltip.show(marker.tooltip_html);
		});

		var path = [];
		var previousPoint = this.gpspoints[0];
		var diff;
		var encodeString;
		for(i=1;i<this.gpspoints.length; i++){
			diff = this.gpspoints[i].timestamp - previousPoint.timestamp;
			if (diff > 10){
				encodeString = google.maps.geometry.encoding.encodePath(path);
				this.pathList.push(this.createPoly({encodedString: encodeString}));
				path = [];

				encodeString = google.maps.geometry.encoding.encodePath([	new google.maps.LatLng(previousPoint.lat, previousPoint.lon),
																			new google.maps.LatLng(this.gpspoints[i].lat, this.gpspoints[i].lon)
																		]);
				this.pathList.push(this.createPoly({encodedString: encodeString, color: "#0000FF", tooltip:true, timediff: diff }));
			}
			path.push(new google.maps.LatLng(this.gpspoints[i].lat, this.gpspoints[i].lon));
			previousPoint = this.gpspoints[i];
		}

		this.pathList.push(this.createPoly({encodedString: google.maps.geometry.encoding.encodePath(path) }));

		console.log("discarded phone states: "+discardedCount);
	},

	createPoly: function(args){
		var lineSymbol = {
	    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
	  };

		var line = new google.maps.Polyline({
										path: google.maps.geometry.encoding.decodePath(args.encodedString),
										// icons: [{
										// 	icon: lineSymbol,
										// 	offset: '100%'
										// }],
										strokeColor: args.color===undefined?"#FF0000":args.color,
										strokeOpacity: 0.5,
										strokeWeight: 3,
										map: this.map,
										visible: false
								});

		if (args.tooltip !== undefined && args.tooltip){
			google.maps.event.addListener(line, 'mouseout', function() {
				tooltip.hide();
		    this.setOptions({
		       strokeOpacity: 0.5,
		       icons: null
		    });
			});
			google.maps.event.addListener(line, 'mousemove', function() {
				tooltip.show("<strong>No data</strong><br /><p>Time distance :"+args.timediff+" seconds</p>");
		    this.setOptions({
		      strokeOpacity: 1,
		      icons: [{
		        icon: lineSymbol,
		        offset: '100%'
		      }]
		    });
			});
		}

		return line;
	},

	showMarkers: function(){
		var i;
		for(i=0;i<this.markersList.length; i++){
			this.markersList[i].setVisible(true);
			this.markersList[i].setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
		}
	},

	showPaths: function(){
		var i;
		for(i=0;i<this.pathList.length; i++){
			this.pathList[i].setVisible(true);
			this.pathList[i].setOptions({zIndex: (google.maps.Marker.MAX_ZINDEX + 1) });
		}
	},

	hideMarkers: function(){
		var i;
		for(i=0;i<this.markersList.length; i++){
			this.markersList[i].setVisible(false);
		}
	},

	hidePaths:function(){
		var i;
		for(i=0;i<this.pathList.length; i++){
			this.pathList[i].setVisible(false);
		}
	},

	show: function(){
		this.showPaths();
		this.showMarkers();
	},


	hide: function(){
		this.hidePaths();
		this.hideMarkers();
	},

	unloadMakers: function(){
		var i;
		for(i=0;i<this.markersList.length; i++){
			this.markersList[i].setMap(null);
			this.oms.removeMarker(this.markersList[i]);
		}
		this.markersList = [];

		for(i=0;i<this.pathList.length; i++){
			this.pathList[i].setMap(null);
		}
		this.pathList = [];
	},

	createMarker: function(lat, lng, args, map){
		var myLatlng = new google.maps.LatLng(lat , lng);

		var marker = new google.maps.Marker({
			position: myLatlng,
			visible: false
		});

		marker.setMap(map);
		this.oms.addMarker(marker);
		marker.tooltip_html = "<strong>Phone State</strong><br /><p>id:"+args.id+" idx:"+args.idx+" state:"+args.state+"<br/>msg:"+args.msg+"<br/>offset: "+args.offset+"s</p>";
		//console.log("adding marker for phone_state: "+args.idx);

		return marker;
	}

};
