class AddDisabilityRelatedColumnsToQuestionnaire < ActiveRecord::Migration
  def change
    add_column :pre_survey_socio_economic_characteristics, :disability_status_bus, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_underground, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_tram, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_suburban_rail, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_rail, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_taxi, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_cycling, :integer
    add_column :pre_survey_socio_economic_characteristics, :disability_status_walking, :integer
  end
end
