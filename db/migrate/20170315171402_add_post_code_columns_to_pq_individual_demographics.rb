class AddPostCodeColumnsToPqIndividualDemographics < ActiveRecord::Migration
  def change
    add_column :pq_individual_demographics, :home_post_code, :string
    add_column :pq_individual_demographics, :work_post_code, :string
  end
end
