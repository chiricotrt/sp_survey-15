class RemovePostCodeColumnsFromQuestionnaire < ActiveRecord::Migration
  def change
    remove_column :pre_survey_socio_economic_characteristics, :home_post_code, :string
    remove_column :pre_survey_socio_economic_characteristics, :work_post_code, :string
  end
end
