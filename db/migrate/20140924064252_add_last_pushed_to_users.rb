class AddLastPushedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_pushed, :timestamp
  end
end
