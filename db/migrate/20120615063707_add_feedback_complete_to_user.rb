class AddFeedbackCompleteToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :feedback_complete, :integer
  end

  def self.down
    remove_column :users, :feedback_complete
  end
end
