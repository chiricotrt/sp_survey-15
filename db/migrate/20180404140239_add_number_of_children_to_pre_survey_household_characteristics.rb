class AddNumberOfChildrenToPreSurveyHouseholdCharacteristics < ActiveRecord::Migration
  def change
    add_column :pre_survey_household_characteristics, :number_of_children, :integer
  end
end
