class AddHintToAddons < ActiveRecord::Migration
  def change
    add_column :addons, :hint, :string
  end
end
