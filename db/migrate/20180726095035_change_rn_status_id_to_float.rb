class ChangeRnStatusIdToFloat < ActiveRecord::Migration
  def change
    change_column :users, :rn_status, :float
  end
end
